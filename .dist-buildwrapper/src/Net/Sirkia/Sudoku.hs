{- Surkea yritys tehd� Sudoku solver
 - by Marko Sirki�
-}
module Net.Sirkia.Sudoku where

import qualified Data.Vector as Vector
import Data.Char (digitToInt)
import qualified Data.Set as Set
import qualified Data.IntPSQ as PSQ
import Debug.Trace (trace)

type CellPrio = Int -- 0 .. 9, 0 = no candq left, conflict
type CellValue = Int -- 0 .. 9, 0 = cell value not set
type CellIndex = Int -- 0 .. 80 
type CandSet = Set.Set CellValue
type CandQueue = PSQ.IntPSQ CellIndex CandSet
data Board = Board { vals :: Vector.Vector CellValue 
                   , candq :: CandQueue
                   } deriving (Show)

--------------------------------------
-- Static neighbor index table, calculated only once
--
makeNeighborIndices :: CellIndex -> [CellIndex]
makeNeighborIndices idx = let
    r = idx `quot` 9
    c = idx `mod` 9
    br = r `quot` 3 :: Int
    bc = c `quot` 3 :: Int
    row_indices = [x | i <- [0..8], let x = r * 9 + i] 
    col_indices = [y | i <- [0..8], let y = i * 9 + c]
    block_indices = [x | i <- [br*3 .. ((br+1)*3)-1], j <- [bc*3 .. ((bc+1)*3)-1], let x = i*9 + j]
    nset = Set.fromList (row_indices ++ col_indices ++ block_indices)
    in Set.toList $ Set.delete idx nset  

neighborTable :: Vector.Vector [CellIndex]
neighborTable = Vector.fromList [s | i <- [0..80], let s = makeNeighborIndices i]

--------------------------------------
-- Logic

emptyBoard :: Board
emptyBoard = 
    Board { vals = Vector.replicate 81 0,
            candq = PSQ.fromList (zip3 [0..81] (replicate 81 9) (replicate 81 (Set.fromList [1..9])))}

removeCandFromCellAt :: CellIndex -> CellValue -> CandQueue -> CandQueue 
removeCandFromCellAt i n q = case PSQ.lookup i q of 
    Nothing -> q
    Just (_, oldcandset) ->
        let newcandset = Set.delete n oldcandset
            newprio = Set.size newcandset
        in PSQ.insert i newprio newcandset q

decrNeighbors :: CellIndex -> CellValue -> CandQueue -> CandQueue
decrNeighbors idx n queue = 
    foldl (\q i -> removeCandFromCellAt i n q) queue (neighborTable Vector.! idx)

-- Return new board with given cell set 
setValue :: CellIndex -> CellValue -> Board -> Board
-- setValue i n b | trace ("setValue: i=" ++ show i ++ " n=" ++ show n ++ " q.size=" ++ show (PSQ.size (candq b))) False = undefined
setValue i n b = 
    Board {vals = vals b Vector.// [(i, n)], 
           candq = decrNeighbors i n (PSQ.delete i (candq b))}

-- The 'some' function is a version of 'map' runs until first 
-- mapped element is not Nothing. Returns Nothing, if list is exhausted
some :: (a -> Maybe b) -> [a] -> Maybe b
some _ [] = Nothing
some f (x:xs) = case f x of {Nothing -> some f xs; Just b -> Just b}

solve :: Board -> Maybe Board
-- solve b | trace ("solve: q.size = " ++ show (PSQ.size (candq b))) False = undefined
solve b = case PSQ.findMin (candq b) of
    Nothing -> Just b -- solved
    Just (i, candcount, candset) -> 
        if candcount == 0
        then Nothing -- conflict, must backtrack
        else some (\n -> solve (setValue i n b)) (Set.toList candset)     

--------------------------------------
-- Testing
                
readBoard :: String -> Board
readBoard ss =
    let indexed_ss = zip [0..] $ map digitToInt ss
    in foldl (\b (i, n) -> if n > 0 then setValue i n b else b) emptyBoard indexed_ss

toString :: Maybe Board -> String
toString b = case b of
    Nothing -> "No solutions found."
    Just x -> concatMap show (Vector.toList (vals x))

printSolution :: Maybe Board -> IO ()
printSolution b = print $ toString b

-- say ":set +s" in ghci session before running to get execution time
test::IO ()
test = 
    let hardestsudokuintheworld = "850002400720000009004000000000107002305000900040000000000080070017000000000036040"
        sudokuwikiunsolvable28 = "600008940900006100070040000200610000000000200089002000000060005000000030800001600"
    in
        -- (printSolution . solve . readBoard) hardestsudokuintheworld
        (printSolution . solve . readBoard) sudokuwikiunsolvable28
 
    

