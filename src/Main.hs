module Main where

import           Control.Applicative     ((<$>))
import           Net.Sirkia.Sudoku
import           System.Environment      (getArgs)

main::IO ()
    [f] <- getArgs
    sudokus <- lines <$> readFile f -- "sudokus.txt"
    mapM_ (printSolution . solve . readBoard) sudokus
